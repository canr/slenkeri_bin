#! /bin/bash
#
# Audits version control repo metadata.

VCS_DIR="/z/templates/default/version_control.group"

# Define parent directories
EE_SITES_DIR="/z/sites"
EE_TEMPLATES_DIR="/z/templates/default"
EE_LIBS_DIR="/z/files/sitewide"
EE_EXTENSIONS_DIR="/z/e5lZD7RR7Ioexq8pH79HZzSSEho5OQ/expressionengine/third_party"
DOTCMS_PLUGINS_DIR="/v/dotcms_3.3.2/plugins"

python "$VCS_DIR/repoaudit.py" -o "$VCS_DIR/repoaudit.csv" "$EE_SITES_DIR" "$EE_TEMPLATES_DIR" "$EE_LIBS_DIR" "$EE_EXTENSIONS_DIR" "$DOTCMS_PLUGINS_DIR"
