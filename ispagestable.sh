#! /bin/bash
#
# A shell script to test whether a given URL is stable.
#
# This script uses curl to make 101 stateless requests to the same URL.
# Those requests are saved to disk and them compared for any differences using
# diff. If any differences are detected then the page is deemed unstable and
# the script will exit with exit code 1. If any of the requests have HTTP
# status code 404 (Not Found) then the script will print a notification,
# continue downloading and diffing the pages, and then exit with status code 2.

# Config
REPS=101
OUTPUT_DIR="$HOME/tmp/is_page_stable"

# Create output directory
if [ -d "$OUTPUT_DIR" ]; then
    echo "Cleaning up old output directory."
    rm -r "$OUTPUT_DIR"
fi
mkdir -p "$OUTPUT_DIR"
echo "Created output directory."

# Clean output directory
rm $OUTPUT_DIR/page*.html &> /dev/null
rm $OUTPUT_DIR/page*.diff &> /dev/null

# Get pages
echo "Downloading pages."
URL=$1
is404=false
for i in $(seq 1 $REPS); do
    file="$OUTPUT_DIR/page$i.html"

    # Download
    curl -s "$1" -o "$file"

    # 404 check
    if [ "$(cat $file | grep '404')" != "" ]; then
        echo "404 detected."
        is404=true
    fi
done

# Diff pages
echo "Generating diffs."
for j in $(seq 2 $REPS); do
    i=$(( j - 1))
    pagediff=$(diff "$OUTPUT_DIR/page$i.html" "$OUTPUT_DIR/page$j.html")
    if [ ! "$pagediff" == "" ]; then
        echo "$pagediff" > "$OUTPUT_DIR/page${i}-page${j}.diff"
    fi
done

# Output and exit
numdiffs=$(ls -l "$OUTPUT_DIR" | grep ".diff" | wc -l)
if [ "$numdiffs" != "0" ]; then
    echo "$numdiffs diffs detected."
    exit 1
fi
if [ $is404 ]; then
    exit 2
fi
