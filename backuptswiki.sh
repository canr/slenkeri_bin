#!/bin/bash
#
# A shell script to mirror the TSWiki MediaWiki instance. 
#
# Uses wget mirror mode to back up TSWiki.
#
# It is assumed that this script will be run over the MSU VPN, as TSWiki is
# restricted to VPN users. You'll have to configure the script in the
# Configuration section below.

# #############
# Configuration
# #############
# User auth
WIKI_DOMAIN=anr
WIKI_USERNAME=
WIKI_PASSWORD=

# Site layout
WIKI_HOSTNAME=tswiki.anr.msu.edu
WIKI_URL="https://${WIKI_HOSTNAME}/wiki/"  # with trailing slash
WIKI_LOGIN_PAGE="index.php?title=Special:UserLogin"
WIKI_START_PAGE="index.php?title=Special:AllPages"

# Script layout
TIMESTAMP=$(date +%F_%T)
WIKI_DUMP_DIR=${HOME}/Desktop/${WIKI_HOSTNAME}_${TIMESTAMP}
WIKI_DUMP_DIR_LOGIN=${WIKI_DUMP_DIR}/login
WIKI_LOGIN_FORM_PATH=${WIKI_DUMP_DIR_LOGIN}/login_form.html
WIKI_LOGIN_RESPONSE_PATH=${WIKI_DUMP_DIR_LOGIN}/login_response.html
COOKIES_FILE=${WIKI_DUMP_DIR}/cookies.txt
WIKI_LOG_DIR=${WIKI_DUMP_DIR}/logs


# ####
# Main
# ####

# Create dirs
for dir in "${WIKI_DUMP_DIR}" "${WIKI_LOG_DIR}" ${WIKI_DUMP_DIR_LOGIN}; do
    if [ ! -d "${dir}" ]; then
        echo "Creating ${dir}"
        mkdir -p "${dir}"
    fi
done

# Get login token
wget \
    -q \
    --keep-session-cookies \
    --save-cookies ${COOKIES_FILE} \
    --directory-prefix=${WIKI_DUMP_DIR} \
    ${WIKI_URL}${WIKI_LOGIN_PAGE} \
    -O ${WIKI_LOGIN_FORM_PATH} 
login_token=$(grep -oP '(?<=type=\"hidden\" name=\"wpLoginToken\" value=\")[a-z0-9]+' ${WIKI_LOGIN_FORM_PATH})
if [ "x$login_token" = "x" ]; then
    echo "Unable to scrape login token"
    exit 1
fi

# Submit login credentials & token
post_data=""
post_data+="wpName=${WIKI_USERNAME}"
post_data+="&wpPassword=${WIKI_PASSWORD}"
post_data+="&wpDomain=${WIKI_DOMAIN}"
post_data+="&wpRemember=1"
post_data+="&wpLoginAttempt=Log in"
post_data+="&wpLoginToken=${login_token}"
wget \
    -q \
    --keep-session-cookies \
    --load-cookies ${COOKIES_FILE} \
    --save-cookies ${COOKIES_FILE} \
    --post-data "${post_data}" \
    --directory-prefix=${WIKI_DUMP_DIR_LOGIN} \
    "${WIKI_URL}${WIKI_LOGIN_PAGE}&action=submitlogin&type=login" \
    -O ${WIKI_LOGIN_RESPONSE_PATH}
if [ "x$(grep 'dbToken' ${COOKIES_FILE})" = "x" ]; then
    echo "Unable to log in"
    exit 2
fi

# Mirror
echo "=== Mirroring ==="
wget \
    --mirror \
    --recursive \
    --page-requisites \
    --no-verbose --show-progress \
    --output-file="${WIKI_LOG_DIR}/${TIMESTAMP}.log" \
    --rejected-log="${WIKI_LOG_DIR}/${TIMESTAMP}-rejects.log" \
    -e robots=off \
    --timestamping \
    --convert-links \
    --adjust-extension \
    --keep-session-cookies \
    --load-cookies ${COOKIES_FILE} \
    --restrict-file-names=nocontrol \
    --remote-encoding=utf-8 \
    --user-agent=firefox \
    --directory-prefix=${WIKI_DUMP_DIR} \
    --regex-type='pcre' \
    --reject-regex='(Search|Special|Talk)(:|%3A)(?!AllPages)|(action|diff|feed|oldid|useskin)=|printable=yes' \
    ${WIKI_URL}${WIKI_START_PAGE}
