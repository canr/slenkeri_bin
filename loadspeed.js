// Uses Node.js and Nightmare to time how long it takes to load a given page with WebKit.


// Includes
var Nightmare = require('nightmare');
var argv = require('optimist')(process.argv.slice(2))
    .usage('node loadspeed.js <url>')
    .demand(1)
    .alias('n', 'number').describe('n', 'Number of page loads to average.').default('n', 10)
    .argv;


// Get page load times
var address = argv._[0];
console.log('Loading: ' + address);
var startTime = Date.now();
new Nightmare()
  .goto(address)
  .on('loadFinished', function (status) {
    var loadTime = Date.now() - startTime;
    console.log('Load: ' + loadTime);
    loadTimes.push(loadTime);
    console.log(loadTimes);
  })
  .run();
