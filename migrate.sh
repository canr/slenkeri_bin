#! /bin/bash
#
# This script should be sourced from your .bashrc with the following command:
#   . ~/bin/migrate.sh
#
# Copies a set of files on dev to their corresponding locations on prod.

function migrate () {
    # Server mount points
    DIR_DEV='/z/'
    DIR_PROD='/p/'

    # Flags
    backup_prod_files=true

    # Parse options
    local OPTIND    # Necessary because getopts will operate on the global by default, which will prevent it from running after the first use
    local OPTARG    # Ditto
    while getopts b option; do
        case "${option}" in
            b)
                backup_prod_files=false # If flag is set, skip the backup of the production files.
                shift
                ;; 

            --)
                shift
                break
                ;;
        esac
    done

    # Iterate through files
    for path in "$@"; do

        # Make sure file exists
        if [ ! -e $path ]; then
            echo "File '$path' does not exist." 1>&2
            continue
        fi


        # Get full dev path
        dev_path=`abspath $path`

        # Generate prod path
        prod_path=`echo "$dev_path" | sed s#"$DIR_DEV"#"$DIR_PROD"#` # | sed -e 's/\ /\\\ /g'`

        # Make sure target prod directory exists
        prod_dir=`dirname "$prod_path"`
        if [ ! -d "$prod_dir" ]; then
            mkdir -p "$prod_dir"
        fi

        # Backup
        if [[ $backup_prod_files && -e $prod_path ]]; then
            cp "$prod_path" "$prod_path.bak"
        fi

        # Copy
        cp "$dev_path" "$prod_path"

    done
}

function abspath () {
    # generate absolute path from relative path
    # $1     : relative filename
    # return : absolute path
    if [ -d "$1" ]; then
        # dir
        echo "$(cd "$1"; pwd)"
    elif [ -f "$1" ]; then
        # file
        if [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}
