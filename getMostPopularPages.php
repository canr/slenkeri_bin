<?php
/**
 * Ranks pages by pageviews in the past year for all ANR sites.
 *
 * Run with PHP CLI v 5.6
 */

ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');

require realpath('Z:/files/sitewide/Google Analytics API Client/GoogleAnalyticsApiClient.php');

// Create GA client
$gaClient = new GoogleAnalyticsApiClient();

// Define the URL => Google web property ID map
$google_analytics_web_properties = array(
    'http://firewise.msu.edu' => 'UA-11988623-12',
    'http://nativeplants.msu.edu' => 'UA-23758824-15',
    'http://sac.anr.msu.edu' => 'UA-11988623-29',
    'http://www.maeap.org' => 'UA-23758824-23',
    'http://blueberries.msu.edu' => 'UA-23758824-22',
    'http://msue.anr.msu.edu' => 'UA-11988623-50',
    'http://dairystore.msu.edu' => 'UA-11988623-54',
    'http://www.breakfastonthefarm.com' => 'UA-23758824-2',
    'http://demmercenter.msu.edu' => 'UA-23758824-11',
    'http://www.for.msu.edu' => 'UA-23758824-26',
    'http://fsclub.fshn.msu.edu' => 'UA-23758824-27',
    'http://www.psm.msu.edu' => 'UA-23758824-43',
    'http://dairyteam.msu.edu' => 'UA-23758824-24',
    'http://www.ipm.msu.edu' => 'UA-11988623-55',
    'http://fieldcrop.msu.edu' => 'UA-11988623-7',
    'http://www.bsp.msu.edu' => 'UA-11988623-56',
    'http://bestt.spdc.msu.edu' => 'UA-23758824-28',
    'http://www.spdc.msu.edu' => 'UA-23758824-21',
    'http://www.canr.msu.edu' => 'UA-11988623-49',
    'http://pasturedairy.kbs.msu.edu' => 'UA-23758824-30',
    'http://migarden.msu.edu' => 'UA-11988623-19',
    'http://productcenter.msu.edu' => 'UA-23758824-12',
    'http://tollgate.msu.edu' => 'UA-23758824-20',
    'http://od.msue.msu.edu' => 'UA-23758824-32',
    'http://esp.msue.msu.edu' => 'UA-11988623-43',
    'http://cgc.msu.edu' => 'UA-23758824-19',
    'http://glrdc.msu.edu' => 'UA-11988623-2',
    'http://www.nai.msu.edu' => 'UA-23758824-16',
    'http://mg.msue.msu.edu' => 'UA-11988623-42',
    'http://dairynutrition.msu.edu' => 'UA-23758824-44',
    'http://www.ans.msu.edu' => 'UA-11988623-53',
    'http://bioenergy.msu.edu' => 'UA-23758824-8',
    'http://odp.anr.msu.edu' => 'UA-11988623-9',
    'http://grapes.msu.edu' => 'UA-11988623-59',
    'http://www.packaging.msu.edu' => 'UA-11988623-45',
    'http://www.afre.msu.edu' => 'UA-23758824-42',
    'http://www.cpis.msu.edu' => 'UA-11988623-39',
    'http://kelloggchair.anr.msu.edu' => 'UA-11988623-28',
    'http://www.fshn.msu.edu' => 'UA-23758824-39',
    'http://www.mafs-africa.org' => 'UA-23758824-36',
    'http://landpolicy.msu.edu' => 'UA-11988623-65',
    'http://4h.msue.msu.edu' => 'UA-11988623-48',
    'http://agbioresearch.msu.edu' => 'UA-11988623-37',
    'http://www.ent.msu.edu' => 'UA-23758824-41',
    'http://anrweek.canr.msu.edu' => 'UA-11988623-32',
    'http://legumelab.msu.edu' => 'UA-23758824-14',
    'http://hiddenlakegardens.msu.edu' => 'UA-11988623-10',
    'http://michiganlakes.msue.msu.edu' => 'UA-11988623-4',
    'http://apples.msu.edu' => 'UA-23758824-48',
    'http://cherries.msu.edu' => 'UA-23758824-49',
    'http://www.csus.msu.edu' => 'UA-23758824-46',
    'http://bheard.isp.msu.edu' => 'UA-11988623-61',
    'http://www.iflr.msu.edu' => 'UA-23758824-6',
    'http://www.michiganfood.org' => 'UA-23758824-40',
    'http://foodsystems.msu.edu' => 'UA-11988623-62',
    // 'http://www.fw.msu.edu' => 'UA-23758824-51',
    'http://fec.msue.msu.edu' => 'UA-11988623-26'


    // 'http://msp.anr.msu.edu' => 'UA-53783423-5',
    // 'http://adsbm.msu.edu' => 'UA-53783423-2',
    // 'http://www.miffs.org' => 'UA-23758824-45',
    // 'http://www.glla.msu.edu' => 'UA-53783423-4',
);


// Get pageviews by URL
$aggregatedGaData = array();
foreach ($google_analytics_web_properties as $property_url => $property_id)
{
    $data = $gaClient->query(
        $property_id,
        '365daysAgo',
        'yesterday',
        'ga:pageviews',
        array(
            'dimensions' => 'ga:pagePath',
            'max-results' => 10000,
            'sort' => '-ga:pageviews'
        )
    );

    foreach ($data['rows'] as $row)
    {
        $url = "{$property_url}{$row[0]}";
        $pageviewCount = $row[1];

        $aggregatedGaData[$url] = $pageviewCount;
    }
}

// Sort by pageviews high to low
arsort($aggregatedGaData);

// Output
foreach ($aggregatedGaData as $url => $pageviewCount)
{
    echo sprintf("%8d\t%s\n", $pageviewCount, $url);
}


?>
