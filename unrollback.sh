#!/bin/bash
#
# Single-use script to undo the accidental rollback of several pages while
# backing up TSWiki.
# 
# For each page in a directory of TSWiki pages which may have mistakenly been
# rolled back, determine whether the page was rolled back and if so undo the
# rollback action.
#
# It is assumed that this script will be run over the MSU VPN, as TSWiki is
# restricted to VPN users. You'll have to configure the script in the
# Configuration section below.

# #############
# Configuration
# #############
# User auth
WIKI_DOMAIN=anr
WIKI_USERNAME=
WIKI_PASSWORD=

# Site layout
WIKI_HOSTNAME=tswiki.anr.msu.edu
WIKI_URL="https://${WIKI_HOSTNAME}/wiki/"  # with trailing slash
WIKI_LOGIN_PAGE="index.php?title=Special:UserLogin"
WIKI_START_PAGE="index.php?title=Special:AllPages"

# Script layout
WIKI_DUMP_DIR=${HOME}/Desktop/unrollback
WIKI_DUMP_DIR_LOGIN=${WIKI_DUMP_DIR}/login
WIKI_DUMP_DIR_HISTORY=${WIKI_DUMP_DIR}/hist
WIKI_DUMP_DIR_UNDO=${WIKI_DUMP_DIR}/undo
WIKI_DUMP_DIR_UNDONE=${WIKI_DUMP_DIR}/undone
WIKI_LOGIN_FORM_PATH=${WIKI_DUMP_DIR_LOGIN}/login_form.html
WIKI_LOGIN_RESPONSE_PATH=${WIKI_DUMP_DIR_LOGIN}/login_response.html
COOKIES_FILE=${WIKI_DUMP_DIR}/cookies.txt
WIKI_LOG_DIR=${WIKI_DUMP_DIR}/logs


rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}


# ####
# Main
# ####

# Ensure VPN connection. TSWiki is only accessible via VPN.
if [ "x$(pgrep pulsesvc)" = "x" ]; then
    echo "=== Starting VPN ==="
	sudo /etc/NetworkManager/dispatcher.d/10-msu-vpn up up
fi

# Create dirs
for dir in "${WIKI_DUMP_DIR}" "${WIKI_LOG_DIR}" ${WIKI_DUMP_DIR_LOGIN}; do
    if [ ! -d "${dir}" ]; then
        echo "Creating ${dir}"
        mkdir -p "${dir}"
    fi
done

# Get login token
echo "=== Logging in ==="
wget \
    -q \
    --keep-session-cookies \
    --save-cookies ${COOKIES_FILE} \
    --directory-prefix=${WIKI_DUMP_DIR} \
    ${WIKI_URL}${WIKI_LOGIN_PAGE} \
    -O ${WIKI_LOGIN_FORM_PATH} 
login_token=$(grep -oP '(?<=type=\"hidden\" name=\"wpLoginToken\" value=\")[a-z0-9]+' ${WIKI_LOGIN_FORM_PATH})
if [ "x$login_token" = "x" ]; then
    echo "Unable to scrape login token"
    exit 1
fi

# Submit login credentials & token
post_data=""
post_data+="wpName=${WIKI_USERNAME}"
post_data+="&wpPassword=${WIKI_PASSWORD}"
post_data+="&wpDomain=${WIKI_DOMAIN}"
post_data+="&wpRemember=1"
post_data+="&wpLoginAttempt=Log in"
post_data+="&wpLoginToken=${login_token}"
wget \
    -q \
    --keep-session-cookies \
    --load-cookies ${COOKIES_FILE} \
    --save-cookies ${COOKIES_FILE} \
    --post-data "${post_data}" \
    --directory-prefix=${WIKI_DUMP_DIR_LOGIN} \
    "${WIKI_URL}${WIKI_LOGIN_PAGE}&action=submitlogin&type=login" \
    -O ${WIKI_LOGIN_RESPONSE_PATH}
if [ "x$(grep 'dbToken' ${COOKIES_FILE})" = "x" ]; then
    echo "Unable to log in"
    exit 2
fi

# testpages="
# /home/slenkeri/Desktop/tswiki.anr.msu.edu/tswiki.anr.msu.edu/wiki/index.php?title=MI_Money_Health&action=rollback&from=Bonddenn&token=92358f32c1529e7b6a0971814ba0daaa%2B%5C.html
# /home/slenkeri/Desktop/tswiki.anr.msu.edu/tswiki.anr.msu.edu/wiki/index.php?title=KeePass&action=rollback&from=Dlangdon&token=b863988288e3049a802fd1a2c92db460%2B%5C.html
# /home/slenkeri/Desktop/tswiki.anr.msu.edu/tswiki.anr.msu.edu/wiki/index.php?title=Great_Lakes_Leadership_Academy&action=rollback&from=Reavakay&token=cc31cba061fc378be6174e52cb8743ae%2B%5C.html
# "

# Rollback
echo "=== Undoing Rollbacks ==="
# for page in rolled back pages
# for page in $testpages; do
for page in $(ls $HOME/Desktop/tswiki.anr.msu.edu/tswiki.anr.msu.edu/wiki/*rollback*.html); do
    echo

    # get page history
    page_hist_url=$( \
        echo $page \
        | sed -e 's/&.*\.html$/\&action=history/' \
        | sed -e 's/\/home\/slenkeri\/Desktop\/tswiki.anr.msu.edu/https:\//')
    wget \
        -q \
        --timestamping \
        --convert-links \
        --adjust-extension \
        --keep-session-cookies \
        --load-cookies ${COOKIES_FILE} \
        --restrict-file-names=nocontrol \
        --remote-encoding=utf-8 \
        --user-agent=firefox \
        --directory-prefix=${WIKI_DUMP_DIR_HISTORY} \
        ${page_hist_url}

    # Select latest undo link & extract undo form URL
    page_hist_path=$( \
        echo $page \
        | sed -e 's/&.*\.html$/\&action=history/' \
        | sed -e 's/.*\/\(index.php.*\)$/\1/')
    undo_url=$( \
        cat ${WIKI_DUMP_DIR_HISTORY}/${page_hist_path}.html \
        | grep '(cur | ' \
        | grep -e 'November \(1\|2\)' \
        | grep -e 'Reverted edits by .* to last revision by .*' \
        | sed -e 's/.*<span class="mw-history-undo"><a href="\([^"]*\)".*/\1/' \
        | sed -e 's/&amp;/\&/g')

    # Filter out pages which weren't successfully rolled back
    if [ "x${undo_url}" = "x" ]; then
        echo "Skipping ${page_hist_url}"
        continue
    fi
    echo "History URL: ${page_hist_url}"
    echo "Undo URL: ${undo_url}"

    # Get undo form
    wget \
        -q \
        --timestamping \
        --convert-links \
        --adjust-extension \
        --keep-session-cookies \
        --load-cookies ${COOKIES_FILE} \
        --restrict-file-names=nocontrol \
        --remote-encoding=utf-8 \
        --user-agent=firefox \
        --directory-prefix=${WIKI_DUMP_DIR_UNDO} \
        ${undo_url}

    # Parse form data
    urllen=${#WIKI_URL}
    undo_filename=${undo_url:$urllen}
    undo_path="${WIKI_DUMP_DIR_UNDO}/${undo_filename}.html"

    action=$(cat $undo_path | grep 'method="post" action="' | sed -e 's/.*method="post" action="\([^"]*\).*/\1/' | sed -e 's/&amp;/\&/')
    editToken=$(cat $undo_path | grep "wpEditToken" | sed -e 's/.*value="\([^"]*\)" name="wpEditToken".*/\1/')
	encodedEditToken=$(rawurlencode "$editToken")
    section=$(cat $undo_path | grep "wpSection" | sed -e 's/.*value="\([^"]*\)" name="wpSection".*/\1/')
    starttime=$(cat $undo_path | grep "wpStarttime" | sed -e 's/.*value="\([^"]*\)" name="wpStarttime".*/\1/')
    edittime=$(cat $undo_path | grep "wpEdittime" | sed -e 's/.*value="\([^"]*\)" name="wpEdittime".*/\1/')
    scrolltop=$(cat $undo_path | grep "wpScrolltop" | sed -e 's/.*value="\([^"]*\)" name="wpScrolltop".*/\1/')
    autosummary=$(cat $undo_path | grep "wpAutoSummary" | sed -e 's/.*value="\([^"]*\)" name="wpAutoSummary".*/\1/')
    summary=$(cat $undo_path | grep "wpSummary" | sed -e 's/.*value="\([^"]*\)" name="wpSummary".*/\1/')
    summary+=" unrollback"
    textbox1=$(cat $undo_path | grep -Pzo "wpTextbox1\">[^<]*</textarea>" | sed -e 's/wpTextbox1">//' | sed -e 's/<\/textarea>//')

    post_data=""
    post_data+="wpEditToken=${encodedEditToken}"
    post_data+="&wpSection=${section}"
    post_data+="&wpStarttime=${starttime}"
    post_data+="&wpEdittime=${edittime}"
    post_data+="&wpScrolltop=${scrolltop}"
    post_data+="&wpAutoSummary=${autosummary}"
    post_data+="&wpSummary=${summary}"
    post_data+="&wpMinoredit=0"
    post_data+="&wpWatchthis=0"
    post_data+="&wpSave=Save page"
    echo "POST: ${post_data}"
    post_data+="&wpTextbox1=${textbox1}"

    # Submit form
    wget \
        --no-verbose --show-progress \
        --output-file="${WIKI_LOG_DIR}/$(date +%F_%T).log" \
        --keep-session-cookies \
        --load-cookies ${COOKIES_FILE} \
        --save-cookies ${COOKIES_FILE} \
        --post-data "${post_data}" \
        --directory-prefix=${WIKI_DUMP_DIR_UNDONE} \
        "${action}"
    echo "Undone"

done
